//segment of function blur
//sigma declared in the function call
//need to define gaussian function G with var dx dy
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

#define sq(x)((x)*(x))
#define PI (3.14159)
double  G(int dx, int dy, double sigma){
  double g = 0;
  g = (1.0 / (2.0 * PI * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2 * sq(sigma)));
  return g;
}





unsigned char generate_matrix(double sigma)
{
  int n =0;
  int checks = 0;
  if(sigma <1){
    checks = 10*sigma;
  }else if (sigma>=1){
    checks = sigma;
  }
  
  if ((checks%2 != 1){
      n = 10*sigma+1;
    } else if((checks%2)==0){
      n=10*sigma;
    }

  double sum = 0.0;
  unsigned char m[n][n];
  for (int i=-n/2; i<n/2; i++){
    for(int j=-n/2; j<n/2; j++){
      //we want to calculate the value for the upper left hand corner           
      m[i+2][j+2] = G(i, j, sigma);
      sum += m[i+2][j+2];
    }
  }

  for (int i =0; i<n; ++i){
    for (int j = 0; j<n; j++){
      m[i][j] /=sum;
    }
  }
  return m;
}
  

   
/*
//function transfer from 1D to 2D,and get pixel value,such as n position to [x][y]
char trans_get (Image *im,int x,int y,char col){

  char pixel;

  if(col='r'){
    pixel = (char)img->data[(x)*(img->cols))+y].r;
}  
 else if(col='g'){
    pixel = (char)img->data[(x)*(img->cols))+y].g;
}  
 else  if(col='b'){
    pixel = (char)img->data[(x)*(img->cols))+y].b;
}
return pixel;
}


double caculate_pixel(int color,double generate_matrix,int cor,int m,int n,int x_r,int x_l,int y_r,int y_l,Image *img ){
  double sum=0;
  double weight=0;

  for (int x=x_l;x<x_r;x++){
     for (int y=y_l;y<y_r;y++){
       int a=cor+x-m;
       int b=cor+y-n;
       unsigned char pixel=trans_get(img,x,y,color);
       double col_val=(double)pixel;
       sum=sum+(col_val*gen_matr[a][b]);
       weight=weight+gen_matr[a][b];
     }
  }

  double blur_val=(double)(sum/weight);
  return blur_val;
}

double blur_color(double **filter,int n,int )
/*
int main(){

  double m[5][5]= generate_matrix(0.5);
  for(int i = 0; i<5; i++){
    for (int j = 0; j<=5; j++){
      printf("%f ", m[i][j]);
    }
    printf("\n");
  }
  return 0;
}
*/
  //About edges
  Image* edges(Image *image,int sigma, int thresh){
  const unsigned char max = 255;
  const unsigned char  min = 0;    
  Image *ed=malloc(sizeof(Image*));//Do i need this one?
  float intensity = 0;
  float r=0, g=0, b=0;
  //assign same value to all three color channels                              
 
    img1=grayscale(img);
    img1=blur(sigma,img1);
    
    ed->data = malloc(sizeof(Pixel)*(image->cols)*(image->rows));
    ed->rows = image->rows;
    ed->cols = image->cols;//need to check struct
   for (int i=0; i<ed->rows;i++){
     for(int j=0;j<ed->cols;j++){
       
       double I_x=abs((ed->data[i+1].r-ed->data[i-1].r)/2);
       double I_y=abs((ed->data[j+1].r-ed->data[j-1].r)/2);
       I_sum=sqrt(sq(I_x)+sq(I_y));
     }
   
   if (I_sum>thresh){
   ed->data[i]=0;
   ed->data[i]=0;
   ed->data[i]=0;
     }
   else {
   ed->data[i]=255;
   ed->data[i]=255;
   ed->data[i]=255;
   }
   }
  return ed;
  }
     
     
  //How to get intensity ??
  
