//Hangrui Liu
//Xiaokun Yu
//hliu111
//xyu61
#ifndef imageManip
#define imageManip

#include<stdio.h>
#include"ppm_io.h"

/*This is helper function to check and result and clamp the result to the range [0, 255]*/
unsigned char saturation(int bright, double rgb);

/*Function change brightness,will call saturation 
 */
Image* bright(Image* image, int brightness);

//This function by a intensity formula to change all rgb value to one value.
Image* grayscale(Image* image);

//By selection colons and rows to crop one picture
Image* crop (int topC, int topR, int botC, int botR,Image *image);

//The function specify the two corners of the section  to blackout
Image* blackout(int topC, int topR, int botC, int botR,Image *image);

//This is a gaussian formula
double G(int dx,int dy,double sigma);

//To get the pixel color
unsigned char getcolor(int i,int j,char color, Image *image);

//deal the different filter cases passing the image border
double normalize(int nhalve, int x, int y, double **kernel, int xinit, int yinit, int xfin, int yfin, char color, Image *image);

//To generate Gaussian filter matrix
double ** matrix(double sigma, int n);

//
double normval(int nhalve, int i, int j, int n,  double **kernel, char color, Image *image);

Image* blur(Image * image,double sigma);

Image* edges(Image *image,int sigma, int thresh);






#endif
