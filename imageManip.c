//Hangrui Liu
//Xiaokun Yu
//hliu111
//xyu61
#include<stdio.h>
#include<string.h>
#include<math.h>
#include<stdlib.h>
#include<assert.h>
#include "ppm_io.h"
#include "imageManip.h"
#define PI 3.1415926
#define sq(x)((x)*(x))

/*                                                                            
helper function                                                               
 */
unsigned char saturation(int bright, double rgb){
  const unsigned char max = 255;
  const unsigned char  min = 0;
  if((rgb + bright)<0){
    return min;
  }else if((rgb + bright)>255){
    return max;
  }else{
    return (unsigned char) (rgb+ bright);
  }
}

double G(int dx, int dy, double sigma){
  double g = 0;
  g = (1.0 / (2.0 * PI * sq(sigma))) * exp((-(sq(dx) + sq(dy)) / (2 * sq(sigma))));
  return g;
}
/* Brightness                                                                  
 * per pixel operation                                                         
 * need helper function for saturation-return unsigned char                    
 * need to define input size                                                   
 * data is 1D                                                                  
 */
Image* bright (Image *image, int brightness){
  Image *br = malloc(sizeof(Image*));
  br->data = malloc(sizeof(Pixel)*(image->cols)*(image->rows));
  br->rows = image->rows;
  br->cols = image->cols;//need to check struct                                 
  for (int i=0; i<br->rows*br->cols; i++){
    br->data[i].r = saturation(brightness, (double)image->data[i].r);
    br->data[i].g = saturation(brightness, (double)image->data[i].g);
    br->data[i].b = saturation(brightness, (double)image->data[i].b);
  }
  return br;
}
Image* grayscale(Image *image){
  Image *gr = malloc(sizeof(Image*));
  gr->data = malloc(sizeof(Pixel)*(image->cols)*(image->rows));
  gr->rows = image->rows;
  gr->cols = image->cols;//need to check struct
  float intensity = 0;
  float r=0, g=0, b=0;
  //assign same value to all three color channels                              
  for (int i=0; i<gr->rows*gr->cols; i++){
    r = (float)image->data[i].r;
    g = (float)image->data[i].g;
    b = (float)image->data[i].b;
    intensity = 0.3*r+0.59*g+0.11*b;
    gr->data[i].r = (unsigned char) intensity;
    gr->data[i].g = (unsigned char) intensity;
    gr->data[i].b = (unsigned char) intensity;
  }
  return gr;
}

Image* crop (int topC, int topR, int botC, int botR, Image* image){
  int xlen = botR-topR+1;
  int ylen = botC-topC+1;
  int cropsize = xlen * ylen;
  //reserve space for cropped image                                            
  Image *cropped = (Image *) malloc(sizeof(image));
  cropped->data = malloc(sizeof(Pixel)*xlen*ylen);
  //if the cropped area is greater than the original size, then return the original image.                                                                    
  if (xlen >= (image->rows)|| ylen >= (image->cols)){
      return image;
    }else if (cropped->data == NULL){
      return NULL;
    }
      cropped->rows = xlen;
      cropped->cols = ylen;
      cropped->data = malloc(sizeof(Pixel)*cropsize);
  int n = 0;
  for (int i=topR; i<botR+1; i++){
    for(int j =topC; j<botC+1; j++){
      cropped->data[n] = image->data[((i)*(int)(image->cols))+j];
      //convert [row][col] to [rol*col + col]
      n++;
    }
  }
    return cropped;
}

Image* blackout(int topC, int topR, int botC, int botR, Image * image){
  Image* occlude = malloc(sizeof(Image*));
  occlude->rows = image->rows;
  occlude->cols = image->cols;
  occlude->data = malloc(sizeof(int)*(image->rows)*(image->cols));
  int n = 0;
  int ylen = botC-topC +1;
  for(int i =0; i<(int)(image->rows); i++){
    for (int k =0; k<(int)(image->cols); k++){
      if((i>=topR)&&(i<botR+1)&&(k>=topC)&&(k<botC+1)){
        occlude->data[i*ylen].r =0;
        occlude->data[i*ylen].g =0;
        occlude->data[i*ylen].b = 0;
      }else{
	occlude->data[n].r = image->data[n].r;
	occlude->data[n].g = image->data[n].g;
	occlude->data[n].b = image->data[n].b;
      }
      n++;
    }
  }
  return occlude;

}
double normalize(int nhalve, int x, int y, double **kernel, int xinit, int yinit, int xfin, int yfin, char color, Image *image){
  double sum =0;
  double w = 0;
  for (int i = xinit; i<xfin; i++){
    for (int j = yinit; j<yfin; j++){
      int dx =nhalve+ x - i; //nhalve +  i - x 是否更为自然 
      int dy =nhalve+ y - j; //nhalve +  i - y 是否更为自然 
     
    
      unsigned char pixel = getcolor(i, j, color, image);
      double value = (double) pixel;
      sum+= (value * kernel[dx][dy]);
      w +=kernel[dx][dy];
    }
  }
  double wavg = (double) sum/w;
  return wavg;
}

//want to generate matrix.
double ** matrix(double sigma, int n){

  int dx = 0;
  int dy = 0;
  double g = 0;
  //kenral is double 
  double  **kernel = (double **) malloc(sizeof(double *) *n); 
  for(int i =0; i<n; i++){
    
    kernel[i] = (double *)malloc(sizeof(double)*n);
  }
  for (int i=0; i<n; i++){
    for(int j=0; j<n; j++){
      dx = i-(n/2);
      dy = j-(n/2);
      g = G(dx, dy, sigma);
      kernel[i][j]=g;
      printf("%lf ",g);
      }
    printf("\n");
  }
  return kernel; 
}

//want to assign color to pixel
unsigned char getcolor(int i, int j, char color, Image *image){
  unsigned char pixel;
  if (color == 'r'){
    //transfer to 1D
    pixel = (unsigned char) image->data[i*(image->cols)+j].r;
  }else if(color == 'g'){
    pixel = (unsigned char) image->data[i*(image->cols)+j].g;
  }else if(color == 'b'){
    pixel = (unsigned char) image->data[i*(image->cols)+j].b; 
  }
  return pixel;
}
 
double normval(int nhalve, int i, int j, int n,  double **kernel, char color, Image *image){
  double nval = 0;
  int xinit =0,yinit = 0,xfin = 0,yfin = 0;
  
  if(i<nhalve){ //i in front 
  	xinit = 0;
  	xfin = i+ nhalve;
    if(j<nhalve){  //j in front 
      yinit = 0;
	  yfin = j + nhalve;
    }else if (j>(image->rows)-nhalve-1){ //j in tail
      yinit = j-nhalve;
      yfin = image->rows;
    }else { //j in center
      yinit = j-nhalve;
      yfin = j + nhalve ;
    }
  }else if (i>=nhalve && i<((image->cols)-nhalve-1)){ //i in center 
    xinit = i - nhalve;
    xfin =  i + nhalve;
    if (j<nhalve){ //j in front
      yfin = 0; 
      yfin = j + nhalve;
    }else if(j>((image->rows)-nhalve-1)){ //j in tail  
	  yinit = j-nhalve;
	  yfin = image->rows;
    }else { //j in center
	  yinit = j - nhalve;
	  yfin = j + nhalve;
    }
  }else{ // i in tail  
      xinit = i-nhalve;
      xfin = image->cols;
	if(j<nhalve){ //j in front 
	  yinit = 0;
	  yfin = j + nhalve;
	}else if (j>((image->rows)-nhalve-1)){ //j in tail  
	  yinit = j-nhalve;
	  yfin = image->rows;
	}else{ //j in center
	  yinit = j- nhalve;
	  yfin = j + nhalve;
	  }
    }
  xfin=i+nhalve;
  yfin=j+nhalve;
  nval = normalize(nhalve, i, j, kernel, xinit, yinit, xfin, yfin, color, image);
    return nval;
}
Image* blur(Image * image,double sigma){

  Image * blurred = malloc(sizeof(Image*));
  blurred->rows = image ->rows;
  blurred->cols = image ->cols;
  blurred->data = malloc(sizeof(Pixel)*(image->rows)*(image->cols));
  int n = 0;
  int checks = 0; //want to check image size
  
    checks = 10*sigma;
  

  if ((checks%2) !=0 ){
    n = checks;
    
  }else if((checks%2)==0){
    n =checks +1;
  }
  //copy the image into blurred
  /*  for(int i=0; i<(image->rows)*(image->cols); i++){
    blurred->data[i].r = image->data[i].r;
    blurred->data[i].g = image->data[i].g;
    blurred->data[i].b = image->data[i].b;
    }*/
  double r=0;
  double g=0;
  double b = 0;
  double **kernel = matrix(sigma, n); 
  for (int j=0; j<(image->cols); j++){
    for(int i =0; i < (image->rows); i++){
      r = normval((int)n/2, i, j, n, kernel, 'r', image);
      g = normval((int)n/2, i, j, n, kernel, 'g', image);
      b = normval((int)n/2, i, j, n, kernel, 'b', image);
      
      blurred->data[i*(blurred -> cols)+j].r =  saturation(0,r);
      blurred->data[i*(blurred -> cols)+j].g =  saturation(0,g);
      blurred->data[i*(blurred -> cols)+j].b =  saturation(0,b);
      
    }
    
  }
  for(int i=0;i<n;i++){
    free(kernel[i]);
  }
  free(kernel);
  // free(image);
  return blurred;
}


Image* edges(Image *image,int sigma, int thresh){

  //const unsigned char max = 255;
  //const unsigned char min = 0;    
Image *ed=malloc(sizeof(Image*));//Do i need this one?,because i malloc blur and gray
//float intensity = 0;
//float r=0, g=0, b=0;
  //assign same value to all three color channels
 
 Image* img1;
 Image* img2;
 
 // img2=blur(img1,sigma);
 img1=grayscale(image);
 img2=blur(img1,sigma);
 
 
 //free(img1->data);
 // free(img1);

 
 ed->data = malloc(sizeof(Pixel)*(image->cols)*(image->rows));
 ed->rows = img2->rows;
 ed->cols = img2->cols;

 
 for (int i=1; i<img2->rows-1;i++){
   for(int j=1;j<img2->cols-1;j++){
     double I_x=abs((img2->data[i*img2->cols+j+1].r-img2->data[i*img2->cols+j-1].r)/2);
     double I_y=abs((img2->data[(i+1)*img2->cols+j].r-img2->data[(i-1)*img2->cols+j].r)/2);
     double I_sum=sqrt(sq(I_x)+sq(I_y));     
   
   //there is a problem,scope problem 
     if (I_sum>thresh){
       ed->data[i*img2->cols+j].r=0;//black
       ed->data[i*img2->cols+j].g=0;
       ed->data[i*img2->cols+j].b=0;
     }
   else {
     ed->data[i*img2->cols+j].r=255;//white 
     ed->data[i*img2->cols+j].g=255;
     ed->data[i*img2->cols+j].b=255;
   }
   }
 }

 return ed;
}



