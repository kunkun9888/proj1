
// __Add your name and JHED above__
// ppm_io.c
// 601.220, summer 2020
// Starter code for midterm project - feel free to edit/add to this file
//Hangrui Liu Xiaokun yu
//hliu111 xyu61
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include <assert.h>
#include "ppm_io.h"



/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *fp) {

  // check that fp is not NULL
  assert(fp); 
  //Is this has a problem??No prpblem,move forwar to next
  char header_1=fgetc(fp);  
  char header_2=fgetc(fp);
  if(header_1!='P'||header_2!='6'){
    return NULL;
  }
  char symbol=fgetc(fp);
  //skip comment line
  if(symbol=='#'){
    while(symbol!='\n'){
    symbol=fgetc(fp);//plus if else, 
  }
  }
  //int 
  //memory allocattion for image*image
  Image* image= malloc(sizeof(Image));
  if(image==NULL){
    free(image);
    return NULL;
  }

  //read columns,rows and colors
  int color_type;
  // image->rows same as image.rows
  // image->rows=r;//??is this right or Image* cols
  // image->cols=c;//is this right??
  int num_input=fscanf(fp,"%d %d %d\n",&(image->cols),&(image->rows),&color_type);
  //Do we need close??fp??
  printf("dimension:%d %d",(image->cols),(image->rows));  
  if(num_input!=3){
    //invalid input
    return NULL;
  }
  //invalid color
  if(color_type!=255){
    return NULL;
  }
  
  image->data=malloc(sizeof(Pixel)*(image->cols)*((image->rows)));//
  if(image->data==NULL){
    return NULL;
  }
  //read number of count wheher equal to count
  int tol_read=fread(image->data,sizeof(Pixel), (image->cols)*(image->rows),fp);//binary reading
  if (tol_read != (image->rows)*(image->cols )){
    return NULL;
  }
  
  return image;  //TO DO: replace this stub
  
}


/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

  // check that fp is not NULL
  assert(fp); 

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->cols * im->rows, fp);

  if (num_pixels_written != im->cols * im->rows) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
  }

  return num_pixels_written;
}

