//Xiaokun Yu
//Hangrui Liu
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ppm_io.h"
#include "imageManip.h"

//Check command line format
int main(int argc,char *argv[]){

  //check argument error
  if(argc==1){
    fprintf(stderr,"Failed to supply input filename and output filename.\n");
    return 1;
  }
   if(argc==2){
    fprintf(stderr,"Failed to supply input filename and output filename.\n");
    return 1;
  }
   if(argc==3){
     fprintf(stderr,"No operation name was specified.\n");
     return 4;
  }
   
  
  //read command line
  char *input=argv[1];
  char *output=argv[2];
  char *opre=argv[3];

  //open input file in binary format
  FILE *fp=fopen(input,"rb");
  if (fp==NULL){
    fprintf(stderr,"Specified input file could not be opened.\n");
    return 2;
  }
  
  //read input file
  Image *img = read_ppm(fp);

  
  if (img==NULL){
    fprintf(stderr,"Specified input file is not a properly-formatted PPM file..\n");
    return 3;
  }
  fclose(fp);

  
  //init opr_syn,for easy to switch each situation 
  char opr_syn='x';
  printf("a");
  Image* img1;
  //strcmp check if they are same
  if(!strcmp(opre,"bright")){
    printf("%s",opre);
    opr_syn='b';
  }
  else if(!strcmp(opre,"gray")){
    opr_syn='g';
  }
   else if(!strcmp(opre,"crop")){
    opr_syn='c';
  }
   else if(!strcmp(opre,"occlude")){
    opr_syn='l';
  }
   else if(!strcmp(opre,"blur")){
    opr_syn='u';
   }
   else if(!strcmp(opre,"edges")){
    opr_syn='e';
   }
  //This is for brightness
  if (opr_syn=='b'){
    printf("123");
    //check arguments if too much,we just need 4 for bright
    if(argc!=5){
      fprintf(stderr,"Incorrect number of arguments.\n");
      free(img->data);
      free(img);
      return 5;
    }
    
    //check if all the number is valid, atoi,convert string to int 
    if((*argv[4]!='0')&&atoi(argv[4])==0){
      fprintf(stderr,"Incorrect number of arguments.\n");	
      free(img->data);
      free(img);
      return 5;
	  }
    int brightness=atoi(argv[4]);
    //should I plus pointer??
    img1 = bright(img,brightness);
    
  }
 

  //for grayscale
  if (opr_syn=='g'){
    if(argc>4){
      fprintf(stderr,"Incorrect number of arguments.\n");
      free(img->data);
      free(img);
      return 5;
 }
 img1=grayscale(img);
      }
 
  //This is for crop 
  if (opr_syn=='c'){
    if(argc!=8){
      fprintf(stderr,"Incorrect number of arguments.\n");
      free(img->data);
      free(img);
      return 5;
 }	  
    int top_col=atoi(argv[4]);
    int top_row=atoi(argv[5]);
    int bot_col=atoi(argv[6]);
    int bot_row=atoi(argv[7]);
    for(int i=4;i<8;i++){
      if (atoi(argv[i])==0&&(*argv[i]!='0')){
	fprintf(stderr,"Incorrect kind of argument.\n");
	free(img->data);
	free(img);
	return 5;
	  }
      //match top column and row to down column and row
      if (top_row>bot_row||top_col>bot_col){
	fprintf(stderr,"Incorrect number of arguments or kind of arguments specified for the specified operation11.\n");
	free(img->data);
	free(img);
	return 6;
	  }
      //check if row and col smallier than zero
      if(top_row<0||top_col<0){
	fprintf(stderr,"Incorrect number of arguments or kind of arguments specified for the specified operation22\n");
	free(img->data);
	free(img);
	return 6;
	  } 
      //I do not know here has a problem...
      /*     if(bot_row>img->rows||bot_col>img->cols){
	fprintf(stderr,"Incorrect number of arguments or kind of arguments specified for the specified operation33\n");
	free(img->data);
	free(img);
	return 6;

      } 
      */
      img1=crop(top_col,top_row,bot_col,bot_row,img);
      
      if (img == NULL) {
	fprintf(stderr, "Could not allocate memory for cropped image.\n");
	free(img->data);
	free(img);

	return 8;
      }
     
	}
	//black out function

  }
  //fOR BLACK OUT
  if (opr_syn=='l'){
    if(argc!=8){
      fprintf(stderr,"Incorrect number of arguments.\n");
      free(img->data);
      free(img);
      return 5;
 }	  
    int top_col=atoi(argv[4]);
    int top_row=atoi(argv[5]);
    int bot_col=atoi(argv[6]);
    int bot_row=atoi(argv[7]);
    for(int i=4;i<8;i++){
      if (atoi(argv[i])==0&&(*argv[i]!='0')){
	fprintf(stderr,"Incorrect kind of argument.\n");
	free(img->data);
	free(img);
	return 5;
	  }
      //match top column and row to down column and row
      if (top_row>=bot_row||top_col>=bot_col){
	fprintf(stderr,"Incorrect number of arguments or kind of arguments specified for the specified operation.123\n");
	free(img->data);
	free(img);
	return 6;
	  }
      //check if row and col smallier than zero
      if(top_row<0||top_col<0){
	fprintf(stderr,"Incorrect number of arguments or kind of arguments specified for the specified operation\n");
	free(img->data);
	free(img);
	return 6;
	  } 

      if(top_row>img->rows||top_col>img->cols){
	fprintf(stderr,"Incorrect number of arguments or kind of arguments specified for the specified operation\n");
	free(img->data);
	free(img);
	return 6;

      } 

      img1=blackout(top_col,top_row,bot_col,bot_row,img);
     
      

    }
    
  }
  //For blur
  
  if (opr_syn=='u'){
    //check arguments if too much,we just need 4 for bright
    if(argc!=5){
      fprintf(stderr,"Incorrect number of arguments.\n");
      free(img->data);
      free(img);
      return 5;
    }
    
    //check if all the number is valid, atoi,convert string to int 
    if((*argv[4]!='0')&&atoi(argv[4])==0){
      fprintf(stderr,"Incorrect number of arguments.\n");	
      free(img->data);
      free(img);
      return 5;
	  }
    char *end;
    //change the string to float
    double sigma=strtod(argv[4],&end);

      //sigma >=0
    if(sigma<=0){
      fprintf(stderr,"Incorrect number of arguments or kind of arguments specified for the specified operation.\n");
      return 5;
    }
    
    img1 = blur(img,sigma);
  }

  //For edges
  if (opr_syn=='e'){
    //check arguments if too much,we just need 4 for bright
    if(argc!=6){
      fprintf(stderr,"Incorrect number of arguments.\n");
      free(img->data);
      free(img);
      return 5;
    }
    
    //check if all the number is valid, atoi,convert string to int 
    for(int i=4;i<6;i++){
      if (atoi(argv[i])==0&&(*argv[i]!='0')){
	fprintf(stderr,"Incorrect kind of argument.\n");
	free(img->data);
	free(img);
	return 5;
	  }
    }    
    char *end;
    //change the string to floay
    double sigma=strtod(argv[4],&end);
    int boundary=atoi(argv[5]);
      //sigma >=0
    if(sigma<=0){
      fprintf(stderr,"Incorrect number of arguments or kind of arguments specified for the specified operation.\n");
      return 6;
    }
    img1 = edges(img,sigma,boundary);
    if (img == NULL) {
      fprintf(stderr, "Could not allocate memory for blurred image.\n");
      free(img->data);
      free(img);
      return 8;
    }
  }

  
  FILE *output_file=fopen(output,"wb");
  
  if(output==NULL){
    fprintf(stderr,"Invalid output\n");
    free(img->data);
    free(img);
    fclose(output_file);
    free(img1->data);
    free(img1);
    return 2;
    
  }
 int num = write_ppm(output_file,img1);
 
  if(num==0){
    fprintf(stderr, "Specified output file could not be opened for writing.\n");
    free(img1->data);
    free(img1);
    fclose(output_file);
    return 7;
  }
  
  free(img->data);
  free(img);
  free(img1->data);
  free(img1);
  return 0;
}

	
