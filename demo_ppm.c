
//demo_ppm.c

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "ppm_io.h"
#include "imageManip.h"

int main() {
  /*
  // allocate space for an Image
  Image * im = malloc(sizeof(Image));
  if (!im) {
    fprintf(stderr, "Uh oh. Image allocation failed!\n");
    return 1;
  }

  // specify dimensions for our Image
  im->rows = 100;
  im->cols = 200;
  
  
  // allocate space for array of Pixels
  Pixel *pix = malloc(sizeof(Pixel) * im->rows * im->cols);
  if (!pix) {
    fprintf(stderr, "Uh oh. Pixel array allocation failed!\n");
    free(im);
    return 1;
  }

  // let data field of Image point to the new array
  im->data = pix;
  
  // fill Pixel array by setting all Pixels to same color
  Pixel my_color = {0, 127, 255}; //the color for most pixels
  Pixel white = {255, 255, 255}; //the color for a few pixels
  for (int r = 0; r < im->rows; r++) {
    for (int c = 0; c < im->cols; c++) {
      if ((r + c) % 8 != 0) { 
	im->data[(r * im->cols) + c] = my_color;
      } else {
	im->data[(r * im->cols) + c] = white;
      }
    }
  }

  // write image to disk
  FILE *fp = fopen("demo.ppm", "wb");
  if (!fp) {
    fprintf(stderr, "Uh oh. Output file failed to open!\n");
    free(im->data);
    free(im);
    return 1;
  }
  int num_pixels_written = write_ppm(fp, im);
  fclose(fp);
  printf("Image created with %d pixels.\n", num_pixels_written);
  
  // clean up!
  free(im->data);  // releases the pixel array
  free(im);        // releases the image itself
  */
  //read binary
  
  FILE* fp=fopen("nika.ppm", "rb");
  FILE* fp_tmp=NULL;
  Image* im=read_ppm(fp);

  fp_tmp=fopen("original.ppm","wb");
  write_ppm(fp_tmp,im);
  fclose(fp_tmp);

  // bright                                                                     
  fp_tmp=fopen("bright.ppm","wb");
  Image* bright_im = bright(im,-75);
  write_ppm(fp_tmp,bright_im);
  fclose(fp_tmp);

  // grayscale                                                                  
  fp_tmp=fopen("grayscale.ppm","wb");
  Image* grayscale_im = grayscale(im);
  write_ppm(fp_tmp,grayscale_im);
  fclose(fp_tmp);


  // crop                                                                       
  fp_tmp=fopen("crop.ppm","wb");
  Image* crop_im = crop(60, 15, 600, 565, im);
  write_ppm(fp_tmp,crop_im);
  fclose(fp_tmp);


  // blackout                                                                   
  fp_tmp=fopen("blackout.ppm","wb");
  Image* blackout_im = blackout(250, 280, 530, 370, im);
  write_ppm(fp_tmp,blackout_im);
  fclose(fp_tmp);

  //blur
  fp_tmp=fopen("blur.ppm","wb");
  Image* blur_im = blur(5, im);
  write_ppm(fp_tmp,blur_im);
  fclose(fp_tmp);

  //edge
  fp_tmp=fopen("edges.ppm","wb");
  Image* edges_im = edges(im, 0.5, 8);
  write_ppm(fp_tmp,edges_im);
  fclose(fp_tmp);
  
  fclose(fp);
  return 0;
  
}
  
